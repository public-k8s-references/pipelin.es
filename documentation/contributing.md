# Improving base imagee
- building image using Kaniko
- enforce docker multi-stage build
- validate any installed binary is working 

# Improving pre-commit hooks

Before committing your pre-commit-hook changes, you can clone this repository and locally check your pre-commit-hooks changes by using the `try-repo` feature: 

```
pre-commit try-repo ../pipelin-es/ --all-files
```

There are pre-commit hooks for:
- [kustomize] checking syntax and format on kubernetes declarative files
- [polaris] checking kubernetes declarative files follow best practices
- [trivy] checking (from a manifest) that containers do not have critical vulnerability

# Working on Buhdee

- The Budhee framework and libraries is developed using goland and under source in `pkg` folder
- The command line program being built is under `cmd` folder
- Check / update the Makefile where needed
- The Budhee cli is embedeed in
