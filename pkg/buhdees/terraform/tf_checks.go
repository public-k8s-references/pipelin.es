package terraform_buhdee

import (
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/libraries/io"
	"os"
	"os/exec"
)

type SkipMap struct {
	TfBin           bool
	TfCheckGit      bool
	TfCheckFmt      bool
	TfCheckValidate bool
}

func TerraformChecks(out io.OutputGenerator, proc io.Process, skipme SkipMap) (success bool) {

	if !skipme.TfBin {
		if !checkTerraformExists(out) {
			return false
		}
	}

	if !skipme.TfCheckGit {
		out.Line("[Terraform Check]", "Ensure it is a git repository")
		if _, err := os.Stat("./.git/"); os.IsNotExist(err) {
			out.Error("Must use in the root of the git repository")
			return false
		}
	}

	if !skipme.TfCheckFmt {
		out.Line("[Terraform Check]", "Ensure Code is properly formatted")
		exitCode := proc.Execute("terraform", "fmt", "-check", "-recursive")
		if exitCode != 0 {
			out.Error("Terraform code not formatted properly. Execute 'terraform fmt -recursive'")
			return false
		}
	}

	if !skipme.TfCheckValidate {
		out.Line("[Terraform Check]", "Initialise Terraform without backend and validate")
		exitCode := proc.Execute("terraform", "init", "-backend=false")
		if exitCode != 0 {
			out.Error("Could not initialize Terraform.")
			return false
		}
		exitCode = proc.Execute("terraform", "validate")
		if exitCode != 0 {
			out.Error("Terraform code not valid. Fix the issue.'")
			return false
		}
	}

	return true
}

func checkTerraformExists(out io.OutputGenerator) (success bool) {
	_, err := exec.LookPath("terraform")
	if err != nil {
		out.Error("terraform not found")
		return false
	}
	return true
}
