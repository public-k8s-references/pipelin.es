package terraform_buhdee

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/libraries/io"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/libraries/test"
	"os"
	"testing"
)

func TestUnit_TerraformChecks_GivenNonFormattedCode_ShouldFail(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	processMock := test.NewProcessMock(out)
	skipmap := SkipMap{
		TfBin:           true,
		TfCheckGit:      true,
		TfCheckFmt:      false,
		TfCheckValidate: false,
	}
	prepareAllCommandPassingByDefault(processMock)
	processMock.AddMockExecution("terraform fmt -check -recursive", test.ProcessMockResult{
		ExitCode: 2,
	})

	// action
	success := TerraformChecks(out, processMock, skipmap)

	// assertion
	assert.False(t, success)
}

func TestUnit_TerraformChecks_GivenNonValidatedCode_ShouldFail(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	processMock := test.NewProcessMock(out)
	skipmap := SkipMap{
		TfBin:           true,
		TfCheckGit:      true,
		TfCheckFmt:      false,
		TfCheckValidate: false,
	}
	prepareAllCommandPassingByDefault(processMock)
	processMock.AddMockExecution("terraform validate", test.ProcessMockResult{
		ExitCode: 2,
	})

	// action
	success := TerraformChecks(out, processMock, skipmap)

	// assertion
	assert.False(t, success)
}

func prepareAllCommandPassingByDefault(processMock test.MockProcess) {
	processMock.AddMockExecution("terraform fmt -check -recursive", test.ProcessMockResult{
		ExitCode: 0,
	})
	processMock.AddMockExecution("terraform init -backend=false", test.ProcessMockResult{
		ExitCode: 0,
	})
	processMock.AddMockExecution("terraform validate", test.ProcessMockResult{
		ExitCode: 0,
	})
}
