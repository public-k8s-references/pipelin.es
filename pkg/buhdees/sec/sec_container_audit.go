package sec

import (
	"fmt"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/libraries/io"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os/exec"
)

type SkipMap struct {
	SecTrivyBin         bool
	SecCheckTrivyScan   bool
	SecCheckTrivyReport bool
}

func AuditContainerFromAuditManifest(out io.OutputGenerator, proc io.Process, manifestFile string, reportFolder string, skipme SkipMap) (success bool) {

	if !skipme.SecTrivyBin {
		_, err := exec.LookPath("/pipelines/trivy")
		if err != nil {
			out.Error("/pipelines/trivy not found")
			return false
		}
	}

	manifest := getAuditMap(out, manifestFile)

	if manifest.Images == nil {
		out.Error("[Audit Container From Manifest] Bad Manifest provided")
		return false
	}
	for _, image := range manifest.Images {
		out.Line("[Audit Container From Manifest]", "Scanning image ["+image.Env+"] : "+image.Value)
		if image.Value != "" {
			if !skipme.SecCheckTrivyScan {
				exitCode := proc.Execute("/pipelines/trivy", "--exit-code", "0", "--cache-dir", ".trivycache/", "--severity", "MEDIUM,HIGH", "--no-progress", image.Value)
				if exitCode != 0 {
					out.Error("Container Audit Critical finding found'")
					return false
				}
			}
			if !skipme.SecCheckTrivyReport {
				reportCode := proc.Execute("/pipelines/trivy", "--exit-code", "0", "--cache-dir", ".trivycache/", "--no-progress", "--format", "json", "-o", reportFolder+"/"+image.Env+".json", image.Value)
				if reportCode != 0 {
					out.Error("Container Audit report failed'")
					return false
				}
			}
		}
	}

	return true
}

// ContainerAuditManifest stores the construct for Container Images to be audited
type ContainerAuditManifest struct {
	Images []struct {
		Env   string `yaml:"env"`
		Value string `yaml:"value"`
	} `yaml:"images"`
}

func getAuditMap(out io.OutputGenerator, manifest string) (auditContent ContainerAuditManifest) {

	out.Line("[Audit Container From Manifest]", "opening yaml manifest")

	// Open the manifest file
	file, err := ioutil.ReadFile(manifest)
	if err != nil {
		out.Error(fmt.Sprintf("Error reading YAML file: %s", err))
		return
	}

	// Read yaml from file
	var yamlConfig ContainerAuditManifest
	err = yaml.Unmarshal(file, &yamlConfig)
	if err != nil {
		out.Error(fmt.Sprintf("Error parsing YAML file: %s", err))
	}

	return yamlConfig
}
