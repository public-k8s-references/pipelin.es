package sec

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/libraries/io"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/libraries/test"
	"os"
	"testing"
)

const test_report_folder = "any"
const test_trivy_scan_command = "/pipelines/trivy --exit-code 0 --cache-dir .trivycache/ --severity MEDIUM,HIGH --no-progress any"
const test_trivy_report_command = "/pipelines/trivy --exit-code 0 --cache-dir .trivycache/ --no-progress --format json -o " + test_report_folder + "/any.json any"
const test_path_manifest_incomplete = "data/incomplete_manifest.yaml"
const test_path_manifest_bad = "data/bad_manifest.yaml"
const test_path_manifest_good = "data/good_manifest.yaml"
const test_path_manifest_empty = "data/empty_manifest.yaml"

func TestUnit_Sec_AuditContainerFromAuditManifest_GivenWrongTrivyScanCommand_ShouldFail(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	processMock := test.NewProcessMock(out)
	skipmap := SkipMap{
		SecTrivyBin:         true,
		SecCheckTrivyScan:   false,
		SecCheckTrivyReport: false,
	}
	manifest := test_path_manifest_good
	prepareAllCommandPassingByDefault(processMock)
	processMock.AddMockExecution(test_trivy_scan_command, test.ProcessMockResult{
		ExitCode: 2,
	})

	// action
	success := AuditContainerFromAuditManifest(out, processMock, manifest, test_report_folder, skipmap)

	// assertion
	assert.False(t, success)
}

func TestUnit_Sec_AuditContainerFromAuditManifest_GivenWrongTrivyReportCommand_ShouldFail(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	processMock := test.NewProcessMock(out)
	skipmap := SkipMap{
		SecTrivyBin:         true,
		SecCheckTrivyScan:   false,
		SecCheckTrivyReport: false,
	}
	manifest := test_path_manifest_good
	prepareAllCommandPassingByDefault(processMock)
	processMock.AddMockExecution(test_trivy_report_command, test.ProcessMockResult{
		ExitCode: 2,
	})

	// action
	success := AuditContainerFromAuditManifest(out, processMock, manifest, test_report_folder, skipmap)

	// assertion
	assert.False(t, success)
}

func TestUnit_Sec_AuditContainerFromAuditManifest_GivenBadManifest_ShouldFail(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	processMock := test.NewProcessMock(out)
	skipmap := SkipMap{
		SecTrivyBin:         true,
		SecCheckTrivyScan:   false,
		SecCheckTrivyReport: false,
	}
	manifest := test_path_manifest_bad
	prepareAllCommandPassingByDefault(processMock)

	// action
	success := AuditContainerFromAuditManifest(out, processMock, manifest, test_report_folder, skipmap)

	// assertion
	assert.False(t, success)
}

func TestUnit_Sec_AuditContainerFromAuditManifest_GivenGoodManifest_ShouldSucceed(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	processMock := test.NewProcessMock(out)
	skipmap := SkipMap{
		SecTrivyBin:         true,
		SecCheckTrivyScan:   false,
		SecCheckTrivyReport: false,
	}
	manifest := test_path_manifest_good
	prepareAllCommandPassingByDefault(processMock)

	// action
	success := AuditContainerFromAuditManifest(out, processMock, manifest, test_report_folder, skipmap)

	// assertion
	assert.True(t, success)
}

func TestUnit_Sec_AuditContainerFromAuditManifest_GivenIncompleteManifest_ShouldSucceed(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	processMock := test.NewProcessMock(out)
	skipmap := SkipMap{
		SecTrivyBin:         true,
		SecCheckTrivyScan:   false,
		SecCheckTrivyReport: false,
	}
	manifest := test_path_manifest_incomplete
	prepareAllCommandPassingByDefault(processMock)

	// action
	success := AuditContainerFromAuditManifest(out, processMock, manifest, test_report_folder, skipmap)

	// assertion
	assert.True(t, success)
}

func TestUnit_Sec_AuditContainerFromAuditManifest_GivenEmptyManifest_ShouldFail(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	processMock := test.NewProcessMock(out)
	skipmap := SkipMap{
		SecTrivyBin:         true,
		SecCheckTrivyScan:   false,
		SecCheckTrivyReport: false,
	}
	manifest := test_path_manifest_empty
	prepareAllCommandPassingByDefault(processMock)

	// action
	success := AuditContainerFromAuditManifest(out, processMock, manifest, test_report_folder, skipmap)

	// assertion
	assert.False(t, success)
}

func prepareAllCommandPassingByDefault(processMock test.MockProcess) {
	processMock.AddMockExecution(test_trivy_scan_command, test.ProcessMockResult{
		ExitCode: 0,
	})
	processMock.AddMockExecution(test_trivy_report_command, test.ProcessMockResult{
		ExitCode: 0,
	})
}
