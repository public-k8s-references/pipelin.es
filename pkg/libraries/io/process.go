package io

import (
	"os"
	"os/exec"
	"strings"
)

type process struct {
	output OutputGenerator
}

func NewProcess(out OutputGenerator) Process {
	return &process{output: out}
}

func (p process) Execute(command string, arg ...string) (exitCode int) {
	p.output.Command(command + " " + strings.Join(arg, " "))
	return p.execute(command, arg...)
}

func (p process) ExecuteWithBash(bashCommand string) (exitCode int) {
	p.output.Command(bashCommand)
	return p.execute("bash", "-c", bashCommand)
}

func (p process) execute(command string, arg ...string) (exitCode int) {
	cmd := exec.Command(command, arg...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			return exitError.ExitCode()
		} else {
			panic(err)
		}
	}

	return 0
}
