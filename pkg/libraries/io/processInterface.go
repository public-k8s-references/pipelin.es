package io

type Process interface {
	Execute(command string, arg ...string) (exitCode int)
	ExecuteWithBash(bashCommand string) (exitCode int)
}
