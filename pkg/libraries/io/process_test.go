package io_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/libraries/io"
	"os"
	"testing"
)

func TestUnit_GivenProcess_WhenGoCommandExists_ShouldSucceed(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	proc := io.NewProcess(out)

	// action
	exitCode := proc.Execute("go", "env")

	// assertion
	assert.Equal(t, 0, exitCode)
}

func TestUnit_GivenProcess_WhenCommandDoesNotExist_ShouldPanic(t *testing.T) {
	// prepare
	defer func() { recover() }()
	out := io.OutputGenerator{W: os.Stdout}
	proc := io.NewProcess(out)

	// action
	proc.Execute("binary-command-does-not-exist", "env")

	// assertion
	assert.FailNow(t, "Should panic")
}

func TestUnit_GivenRealProcess_WhenExistingCommdandFails_ShouldReturnNonZeroExitCode(t *testing.T) {
	// prepare
	out := io.OutputGenerator{W: os.Stdout}
	proc := io.NewProcess(out)

	// action
	exitCode := proc.Execute("cat", "file-does-not-exist")

	// assertion
	assert.NotEqual(t, 0, exitCode)
}
