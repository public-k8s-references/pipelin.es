package io

import (
	"fmt"
	"io"
)

//const bold = "\033[1m\033[90m"
const bold = "\033[1m"
const green = "\033[0;32m"
const blue = "\033[0;34m"
const red = "\033[0;31m"
const reset = "\033[0m"
const indent = "    "

type OutputGenerator struct {
	W io.Writer
}

func (g *OutputGenerator) EmptyLine() {
	fmt.Fprintln(g.W)
}

func (g *OutputGenerator) Command(command string) {
	fmt.Fprintln(g.W, green+" $ "+command+reset)
}

func (g *OutputGenerator) Line(prefix string, str string) {
	fmt.Fprintln(g.W, bold+" > "+prefix+" "+str+reset)
}

func (g *OutputGenerator) MultiLine(prefix string, str string, followUpLines ...string) {
	g.EmptyLine()
	fmt.Fprintln(g.W, bold+" |-> "+prefix+": "+str+reset)
	for _, line := range followUpLines {
		fmt.Fprintln(g.W, indent+line)
	}
	fmt.Fprintln(g.W, prefix+str)
}

func (g *OutputGenerator) Success(message string) {
	fmt.Fprintln(g.W, green+" > "+message+reset)
}

func (g *OutputGenerator) Error(message string) {
	fmt.Fprintln(g.W, red+" > "+message+reset)
}
