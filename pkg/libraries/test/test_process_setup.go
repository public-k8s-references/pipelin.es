package test

import (
	"fmt"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/libraries/io"
	"strings"
)

type ProcessMockResult struct {
	ExitCode int
}

// each command result has a ProcessMock result
type processMock struct {
	output  io.OutputGenerator
	results map[string]ProcessMockResult
}

type MockProcess interface {
	io.Process
	AddMockExecution(command string, result ProcessMockResult)
}

func NewProcessMock(out io.OutputGenerator) MockProcess {
	return &processMock{
		output:  out,
		results: map[string]ProcessMockResult{},
	}
}

func (p processMock) AddMockExecution(command string, result ProcessMockResult) {
	p.results[command] = result
}

func (p processMock) Execute(command string, arg ...string) (exitCode int) {
	p.output.Command(command + " " + strings.Join(arg, " "))
	commandString := command + " " + strings.Join(arg, " ")
	result, exists := p.results[commandString]
	if !exists {
		panic(fmt.Errorf("Test Error: no mock result data found for commandString: %s", commandString))
	}
	return result.ExitCode
}

func (p processMock) ExecuteWithBash(bashCommand string) (exitCode int) {
	p.output.Command(bashCommand)
	result, exists := p.results[bashCommand]
	if !exists {
		panic(fmt.Errorf("Test Error: no mock result data found for commandString: %s", bashCommand))
	}
	return result.ExitCode
}
