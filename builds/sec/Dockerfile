ARG GOLANG_VERSION=1.14

FROM alpine:3.12.1 AS base
RUN apk add bash git openssh

FROM base AS build-base

RUN apk update && \
    apk add \
    curl \
    rpm \
    jq

ENTRYPOINT ["sh", "-o", "pipefail", "-c"]

# ======================================================================================================================
# Aquasec Trivy - For Security Scan without db server
# ======================================================================================================================
FROM build-base AS trivy

RUN curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sh -s -- -b /usr/local/bin

# ======================================================================================================================
# YQ - For YAML reading/writing
# ======================================================================================================================
FROM build-base AS yq

ARG ALPINE_EDGE_MIRROR=http://dl-4.alpinelinux.org/alpine/edge/community
RUN echo "$ALPINE_EDGE_MIRROR" >> /etc/apk/repositories

WORKDIR /yq
RUN apk update
RUN apk add yq --no-cache
RUN which yq

# ======================================================================================================================
# BUHDEE - A companion for heavy lifting of usefull pipeline commands
# ======================================================================================================================
FROM golang:${GOLANG_VERSION} as buhdee

ARG VERSION=latest

RUN mkdir -p /buhdee

ADD . /buhdee
RUN cd /buhdee && make -e OUTPUT_DIR=bin COMPANION_BUHDEE=buhdee VERSION=${VERSION}
RUN cp /buhdee/bin/buhdee-${VERSION} /buhdee/bin/buhdee

RUN echo "  Golang version:      `go version`" >> /buhdee/companion-build-info.txt
RUN echo "  Commit hash:         `echo ${CI_COMMIT_SHA}`" >> /buhdee/companion-build-info.txt

# Runtime
FROM base AS base-sec

LABEL maintainer="https://gitlab.com/frederic.fok"

COPY --from=trivy /usr/local/bin/trivy /usr/local/bin/trivy
COPY --from=yq /usr/bin/yq /usr/local/bin/yq
COPY --from=buhdee /buhdee/bin/buhdee /usr/local/bin/buhdee

RUN mkdir /pipelines
RUN ln -s /usr/local/bin/trivy /pipelines/trivy
RUN ln -s /usr/local/bin/yq /pipelines/yq
RUN ln -s /usr/local/bin/buhdee /pipelines/buhdee


RUN /pipelines/trivy help
RUN /pipelines/yq help
RUN /pipelines/buhdee help

ENTRYPOINT ["bash", "-c"]
