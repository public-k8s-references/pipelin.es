# PIPELIN.ES

> PIPELIN-ES IS A DEVSECOPS TOOLKIT COMPANION AND FRAMEWORK FOR CI/CD

## 1. Base Images

## K8s

- CI CD Image for K8s related pipeline workflows
- You may use the following `pipelin.es` commands:
    - [/pipelines/aws-iam-authenticator](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html)
    - [/pipelines/polaris](https://github.com/FairwindsOps/polaris)
    - [/pipelines/kustomize](https://kustomize.io)
    - [/pipelines/kubectl](https://kubectl.docs.kubernetes.io)
    - [/pipelines/yq](https://github.com/mikefarah/yq)
    - [/pipelines/helm](https://helm.sh)
    - [/pipelines/crane](https://github.com/google/go-containerregistry)

## 2. "BUHD-EE" companion

### What is Buhdee?

> Pronounce [ buhd-ee ] / ˈbʌd i /

-   `Buhdee` is a golang pipelin-es companion to make pipelines more declarative, easier to maintain, 
    doing the heavy lifting without having to propagate nor duplicating similar logic in many places
-   By using golang, it makes it portable across systems, usable locally, and more testable than scripts
-   Buhdee does not aim to substitute any other cli or binaries. Instead it reuses or wrap existing ones 
    (terraform, kubetcl, ...) to normalise and solve problems in one place and make it a reusable companion for others.   

### What does Buhdee do?

> This list of functions is extended over time.
> Simply run `/pipelines/buhdee help` for all the options

Here below is a digest of Budhee functionalities:

| Category      | functions     |
| ---           | ---           |
| Terraform     | `terraform-check` - check terraform code |
|               | `terraform-plan` (todo) - plan terraform code |
| Security      | `audit-container` - run security scan on container images based on a Manifest file |


## 3. pre-commit hooks

While most functionalities should (and shall) run in CI/CD pipelines, having pre-commit hooks
can also increase the feedback loops for developers before they even check in their code.

Refer to the `pre-commit-hooks.yaml` for a list of pre-commit hooks (using Docker) available from this repository. 

Adding a pre-commit hook is done on an opt-in basis, you may create a `.pre-commit-config.yaml` file
to configure from which external git repositories your git project shall refer to.
For example, your file could look like:

```
repos:
  -   repo: https://github.com/pre-commit/pre-commit-hooks
      rev: v2.4.0
      hooks:
        -   id: check-yaml
            args: [--allow-multiple-documents]
        -   id: check-added-large-files
    -   repo: https://gitlab.com/public-k8s-references/pipelin.es/
          rev: a34fa3bb1053133faa7503f92f4be27525f6aca0
          hooks:
            -   id: kustomize
                name: kustomize-development
                args: [/src/deployments/overlays/dev]
                verbose: false
```
