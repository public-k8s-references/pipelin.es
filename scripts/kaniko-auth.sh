#!/bin/bash

mkdir -p /kaniko/.docker

echo -e "\033[0;34m writing auths credentials to /kaniko/.docker/config.json \033[0m"
echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
