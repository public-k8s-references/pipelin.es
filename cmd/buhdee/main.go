package main

import (
	"log"
	"os"
)

func main() {
	cli := CreateCli()

	err := cli.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
