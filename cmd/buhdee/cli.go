package main

import (
	"errors"
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/buhdees/sec"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/buhdees/terraform"
	"gitlab.com/public-k8s-references/pipelin.es.git/pkg/libraries/io"
	"os"
	"time"
)

func CreateCli() cli.App {
	defaultTerraformSkipMap := terraform_buhdee.SkipMap{TfBin: false, TfCheckGit: false, TfCheckFmt: false, TfCheckValidate: false}
	defaultSecSkipMap := sec.SkipMap{SecTrivyBin: false, SecCheckTrivyScan: false, SecCheckTrivyReport: false}
	app := cli.NewApp()
	out := io.OutputGenerator{W: os.Stdout}
	proc := io.NewProcess(out)
	app.Name = "Buhdee Companion"
	app.Description = "A Buhdee golang companion to get things delivered declaratively"
	app.Version = "v0.0.1"
	app.Compiled = time.Now()
	app.Commands = []*cli.Command{
		{
			Name:  "terraform-check",
			Usage: "check the terraform code",
			Flags: []cli.Flag{
				&cli.StringFlag{Name: "region", Aliases: []string{"re"}, Value: "eu-west-1", DefaultText: "Ireland"},
			},
			Action: func(c *cli.Context) error {
				out.Line("[var check]", fmt.Sprintf("region: %s", c.String("region")))
				success := terraform_buhdee.TerraformChecks(out, proc, defaultTerraformSkipMap)
				if !success {
					return errors.New("terraform check failed")
				}
				return nil
			},
		},
		{
			Name:  "audit-container",
			Usage: "audit the container images from a Manifest File",
			Flags: []cli.Flag{
				&cli.StringFlag{Name: "file", Aliases: []string{"f"}, Required: true},
				&cli.StringFlag{Name: "out", Aliases: []string{"o"}, Value: "container-scan-reports", DefaultText: "Ireland"},
			},
			Action: func(c *cli.Context) error {
				success := sec.AuditContainerFromAuditManifest(out, proc, c.String("file"), c.String("out"), defaultSecSkipMap)
				if !success {
					return errors.New("audit failed")
				}
				return nil
			},
		},
	}

	return *app
}
