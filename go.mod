module gitlab.com/public-k8s-references/pipelin.es.git

require (
	github.com/stretchr/testify v1.6.1
	github.com/urfave/cli/v2 v2.3.0
	gopkg.in/yaml.v2 v2.3.0
)

go 1.14
