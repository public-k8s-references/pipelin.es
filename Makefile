OUTPUT_DIR := bin
COMPANION_BUHDEE := budhee
VERSION := local

output_buhdee := ${OUTPUT_DIR}/${COMPANION_BUHDEE}-${VERSION}

all: download tidy build_buhdee format test-unit

download:
	@printf -- "\033[37m |-> Download dependencies \033[0m \n"
	@printf -- "\033[34m $     go mod download \033[0m \n"
	@go mod download

tidy:
	@printf -- "\033[37m |-> Tidy dependencies \033[0m \n"
	@printf -- "\033[34m $     go mod tidy \033[0m \n"
	@go mod tidy

build_buhdee:
	@printf -- "\033[37m |-> Building the buhdee companion cli \033[0m \n"
	@printf -- "\033[34m $     go build -o ${output_budhee} ./cmd/buhdee \033[0m \n"
	@go build -o ${output_buhdee} ./cmd/buhdee

format:
	@printf -- "\033[37m |-> Format code \033[0m \n"
	@printf -- "\033[34m $     go fmt ./... \033[0m \n"
	@go fmt ./... > formattedfiles.out

test-unit:
	@printf -- "\033[37m |-> Run unit tests \033[0m \n"
	@printf -- "\033[34m $     go test -run TestUnit ./... \033[0m \n"
	@go test -run TestUnit ./...

report:
	@mkdir -p reports
	@printf -- "\033[37m |-> Find static code issues \033[0m \n"
	@printf -- "\033[34m $     go vet ./... \033[0m \n"
	@go vet ./...
